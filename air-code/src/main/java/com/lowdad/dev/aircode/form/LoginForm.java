package com.lowdad.dev.aircode.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author lowdad
 * 2019-06-05
 **/
@Data
public class LoginForm {

    @NotNull
    private String username;
    @NotNull
    private String password;
}
