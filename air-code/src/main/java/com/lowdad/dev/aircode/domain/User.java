package com.lowdad.dev.aircode.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @author lowdad
 * 2019-06-05
 **/
@Data
@Entity
@Table(name = "air_admin")
@NamedQuery(
        name = "User.findAll",
        query = "SELECT u FROM User u"
)
public class User {
    @Id
    @GeneratedValue(
            generator = "system-native"
    )
    @GenericGenerator(
            name = "system-native",
            strategy = "native"
    )
    @Column(
            name = "user_id"
    )
    private Long userId;

    @Column(
            name = "user_name"
    )
    private String username;

    @Column(
            name = "password"
    )
    private String password;
}
