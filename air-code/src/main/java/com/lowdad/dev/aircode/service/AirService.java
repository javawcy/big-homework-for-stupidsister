package com.lowdad.dev.aircode.service;

import com.lowdad.dev.aircode.form.AirForm;
import com.lowdad.dev.aircode.model.Res;

/**
 * @author lowdad
 * 2019-06-06
 **/
public interface AirService {
    Res add(AirForm airForm);

    Res edit(AirForm airForm);

    Res delete(AirForm airForm);

    Res search(String airId);

    Res airList(String cityName, String start, String end);
}
