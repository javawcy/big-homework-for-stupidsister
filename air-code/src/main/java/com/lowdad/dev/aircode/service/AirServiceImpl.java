package com.lowdad.dev.aircode.service;

import com.lowdad.dev.aircode.domain.Air;
import com.lowdad.dev.aircode.form.AirForm;
import com.lowdad.dev.aircode.model.AirVO;
import com.lowdad.dev.aircode.model.Res;
import com.lowdad.dev.aircode.repository.AirRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author lowdad
 * 2019-06-06
 **/
@Service
public class AirServiceImpl implements AirService{

    @Autowired
    private AirRepository airRepository;

    @Override
    public Res add(AirForm airForm) {
        Air air = new Air();
        air.setCityName(airForm.getCityName());
        air.setCreatedAt(new Date());
        air.setPm(airForm.getPm());

        airRepository.save(air);

        return new Res(0,"success",null);
    }

    @Override
    public Res edit(AirForm airForm) {

        Optional<Air> airOptional = airRepository.findById(Long.valueOf(airForm.getAirId()));
        if (airOptional.isPresent()) {
            Air air = airOptional.get();
            air.setCityName(airForm.getCityName());
            air.setPm(airForm.getPm());
            airRepository.save(air);
        }
        return new Res(0,"success",null);
    }

    @Override
    public Res delete(AirForm airForm) {
        Optional<Air> airOptional = airRepository.findById(Long.valueOf(airForm.getAirId()));
        airOptional.ifPresent(air -> airRepository.delete(air));
        return new Res(0,"success",null);
    }

    @Override
    public Res search(String airId) {
        Optional<Air> airOptional = airRepository.findById(Long.valueOf(airId));
        return airOptional.map(air -> new Res(0, "success", air)).orElseGet(() -> new Res(-1, "不存在", null));
    }

    @Override
    public Res airList(String cityName, String start, String end) {

        Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
        PageRequest pageRequest = PageRequest.of(0, 100, sort);

        Page<Air> page = airRepository.findAll((root, query,cb)->{
            Predicate predicates = cb.and(cb.like(root.get("cityName").as(String.class), "%"+cityName+"%"));
            if (!StringUtils.isEmpty(start)) {
                predicates = cb.and(predicates, cb.greaterThanOrEqualTo(root.get("createdAt").as(String.class), start));
            }
            if (!StringUtils.isEmpty(end)) {
                predicates = cb.and(predicates, cb.lessThanOrEqualTo(root.get("createdAt").as(String.class), end));
            }
            query.where(predicates);
            return query.getRestriction();
        }, pageRequest);

        List<AirVO> list = new ArrayList<>();

        for (Air air : page.getContent()) {
            AirVO airVO = new AirVO();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            BeanUtils.copyProperties(air,airVO);
            airVO.setCreatedAt(sf.format(air.getCreatedAt()));
            list.add(airVO);
        }

        return new Res(0,"success",list);
    }
}
