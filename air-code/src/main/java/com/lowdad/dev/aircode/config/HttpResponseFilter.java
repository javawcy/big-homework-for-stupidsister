package com.lowdad.dev.aircode.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wcy
 * @description 防跨域拦截器
 * @createDate 18-4-9
 * @updateBy
 * @updateDate
 */
@Component
@Slf4j
public class HttpResponseFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("[filter] {}","拦截器启动，添加response请求头");
    }
    /**
     * 全局拦截器，做请求头跨域验证
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response1 = (HttpServletResponse)response;
        response1.setHeader("Access-Control-Allow-Origin", "*");
        response1.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS,DELETE,PUT");
        response1.setHeader("Access-Control-Allow-Headers", "x-requested-with,content-type,token,sign,id,timestamp,session");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
