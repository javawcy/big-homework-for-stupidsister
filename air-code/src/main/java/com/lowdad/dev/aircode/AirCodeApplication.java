package com.lowdad.dev.aircode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(AirCodeApplication.class, args);
    }

}
