package com.lowdad.dev.aircode.repository;

import com.lowdad.dev.aircode.domain.Air;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author lowdad
 * 2019-06-05
 **/
@Repository
public interface AirRepository extends JpaRepository<Air,Long> {

    Page<Air> findAll(Specification<Air> spec, Pageable pageable);
}
