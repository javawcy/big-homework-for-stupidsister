package com.lowdad.dev.aircode.rest;

import com.lowdad.dev.aircode.form.LoginForm;
import com.lowdad.dev.aircode.model.Res;
import com.lowdad.dev.aircode.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lowdad
 * 2019-06-05
 **/
@RestController
public class LoginController {

    @Autowired
    private UserService userService;


    @PostMapping("/user/login")
    public Res login(
            @RequestBody @Validated LoginForm loginForm
    ) {
        return userService.login(loginForm);
    }
}
