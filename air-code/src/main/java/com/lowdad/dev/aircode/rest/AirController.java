package com.lowdad.dev.aircode.rest;

import com.lowdad.dev.aircode.form.AirForm;
import com.lowdad.dev.aircode.model.Res;
import com.lowdad.dev.aircode.service.AirService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author lowdad
 * 2019-06-06
 **/
@RestController
public class AirController {

    @Autowired
    private AirService airService;

    @PostMapping("/air/add")
    public Res add(
            @RequestBody @Validated AirForm airForm
            ) {
        return airService.add(airForm);
    }

    @PostMapping("/air/edit")
    public Res edit(
            @RequestBody @Validated AirForm airForm
    ) {
        return airService.edit(airForm);
    }

    @PostMapping("/air/delete")
    public Res delete(
            @RequestBody @Validated AirForm airForm
    ) {
        return airService.delete(airForm);
    }

    @GetMapping("/air/search")
    public Res search(
            @RequestParam("airId") String airId
    ) {
        return airService.search(airId);
    }


    @GetMapping("/air/list")
    public Res add(
            @RequestParam("version") String version,
            @RequestParam(value = "cityName",required = false) String cityName,
            @RequestParam(value = "start",required = false) String start,
            @RequestParam(value = "end",required = false) String end
    ) {
        return airService.airList(cityName,start,end);
    }
}
