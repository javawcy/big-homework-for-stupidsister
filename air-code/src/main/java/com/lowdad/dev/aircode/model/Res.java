package com.lowdad.dev.aircode.model;

import lombok.Data;

/**
 * @author lowdad
 * 2019-06-05
 **/
@Data
public class Res {

    private int code;
    private String msg;
    private Object data;

    public Res(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
