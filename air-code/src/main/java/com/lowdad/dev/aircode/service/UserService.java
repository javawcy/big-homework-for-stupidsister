package com.lowdad.dev.aircode.service;

import com.lowdad.dev.aircode.form.LoginForm;
import com.lowdad.dev.aircode.model.Res;

/**
 * @author lowdad
 * 2019-06-05
 **/
public interface UserService {
    Res login(LoginForm loginForm);
}
