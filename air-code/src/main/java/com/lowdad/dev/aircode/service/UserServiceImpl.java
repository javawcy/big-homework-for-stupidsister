package com.lowdad.dev.aircode.service;

import com.lowdad.dev.aircode.domain.User;
import com.lowdad.dev.aircode.form.LoginForm;
import com.lowdad.dev.aircode.model.Res;
import com.lowdad.dev.aircode.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lowdad
 * 2019-06-05
 **/
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Res login(LoginForm loginForm) {

        User user = userRepository.findByUsername(loginForm.getUsername());
        if ( user == null) {
            return new Res(-1,"账户不存在",null);
        }

        if (!user.getPassword().equals(loginForm.getPassword())) {
            return new Res(-1,"密码错误",null);
        }

        return new Res(0,"success",null);
    }
}
