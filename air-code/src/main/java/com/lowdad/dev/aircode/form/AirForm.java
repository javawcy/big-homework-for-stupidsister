package com.lowdad.dev.aircode.form;

import lombok.Data;

/**
 * @author lowdad
 * 2019-06-06
 **/
@Data
public class AirForm {

    private String airId;

    private String cityName;

    private String pm;
}
