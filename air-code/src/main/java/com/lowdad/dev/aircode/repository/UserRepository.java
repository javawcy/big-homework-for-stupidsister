package com.lowdad.dev.aircode.repository;

import com.lowdad.dev.aircode.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author lowdad
 * 2019-06-05
 **/
@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsername(String username);
}
