package com.lowdad.dev.aircode.model;

import lombok.Data;

/**
 * @author lowdad
 * 2019-06-06
 **/
@Data
public class AirVO {

    private Long airId;
    private String cityName;
    private String pm;
    private String createdAt;
}
