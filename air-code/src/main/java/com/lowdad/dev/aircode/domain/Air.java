package com.lowdad.dev.aircode.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * @author lowdad
 * 2019-06-05
 **/
@Data
@Entity
@Table(name = "air_quality")
@NamedQuery(
        name = "Air.findAll",
        query = "SELECT a FROM Air a"
)
public class Air {

    @Id
    @GeneratedValue(
            generator = "system-native"
    )
    @GenericGenerator(
            name = "system-native",
            strategy = "native"
    )
    @Column(
            name = "air_id"
    )
    private Long airId;

    @Column(
            name = "created_at"
    )
    private Date createdAt;

    @Column(
            name = "city_name"
    )
    private String cityName;

    @Column(
            name = "pm"
    )
    private String pm;
}
