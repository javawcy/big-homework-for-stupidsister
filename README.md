步骤：

1.安装git

2.在需要下载的目录下执行
```git

git clone https://gitlab.com/javawcy/big-homework-for-stupidsister.git

```

3.air-view 为前端项目，用文本编辑器打开根目录下pages文件夹 login.html 为登陆首页

4.air-code 为java后端项目，导入idea即可

5.运行docker启动数据库镜像，确保有名为 air_quality 的数据库，清空表

6.先运行后端java程序，会自动创建数据库表

7.运行完不报错，在数据库中执行

```sql

insert into air_admin(user_name,password) values ('admin','123');


```

初始化登陆账号

8.用编辑器打开 前端项目的login.html 页面登陆即可

9.如果没有编辑器，找到login.html所在文件夹，右键打开方式选择google chrome